﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardedSkipButton : RewardedButton
{
    public override void CollectReward(){
        LevelManager.Instance.SkipLevel(); // Whats the button should do, the override will play the ads
        // Call the function "Show Rewarded Video" on the Unity Event panel. The string parametter is only for debug
    }
}
