﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathFunction
{
    public static bool IsBetween(float testValue, float bound1, float bound2)
    {
        // Check is a value is between two bound
        return (testValue >= Mathf.Min(bound1, bound2) && testValue <= Mathf.Max(bound1, bound2));
    }

    public static float ClampAngle(float angle, float from, float to)
    {
        // accepts e.g. -80, 80
        if (angle < 0f) angle = 360 + angle;
        if (angle > 180f) return Mathf.Max(angle, 360 + from);
        return Mathf.Min(angle, to);
    }

    public static bool IsInside(Collider c, Vector3 point)
    {
        Vector3 closest = c.ClosestPoint(point);
        // Because closest=point if point is inside - not clear from docs I feel
        return closest == point;
    }

    public static bool isCubeContains(BoxCollider collider, Vector3 point){
        // Only work with cube, instead use IsInside()
        return collider.bounds.Contains(point);
    }

    public static string NiceTimeFormat(float timeToFormat){
        int minutes = Mathf.FloorToInt(timeToFormat / 60F);
        int seconds = Mathf.FloorToInt(timeToFormat - minutes * 60);

        // This will give you times in the 0:00 format. If you'd rather have 00:00, simply do
        // string niceTime = string.Format("{0:00}:{1:00}", minutes, seconds);

        string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);
        return niceTime;

        
    }

    public static float Remap (this float from, float fromMin, float fromMax, float toMin,  float toMax)
    {
        var fromAbs  =  from - fromMin;
        var fromMaxAbs = fromMax - fromMin;      
       
        var normal = fromAbs / fromMaxAbs;
 
        var toMaxAbs = toMax - toMin;
        var toAbs = toMaxAbs * normal;
 
        var to = toAbs + toMin;
       
        return to;
    }

    private static System.Random rng = new System.Random();
    public static void ShuffleList<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }




}
