﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DrawPathNavMeshAgent : MonoBehaviour
{
    private LineRenderer lineRenderer;
    public NavMeshAgent agentToDebug;
    public bool hasPath;

    private void Start() {
        lineRenderer = GetComponent<LineRenderer>();
    }
   
    void Update()
    {
        if(agentToDebug){
            hasPath = agentToDebug.hasPath;

            if(agentToDebug.hasPath){
                lineRenderer.positionCount = agentToDebug.path.corners.Length;
                lineRenderer.SetPositions(agentToDebug.path.corners);
                lineRenderer.enabled = true;
            }else{
                lineRenderer.enabled = false;
            }
        }
    }
}
