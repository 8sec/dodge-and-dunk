﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollControlleur : MonoBehaviour
{
    /*
    Convert Maximamo Rigs to Unity Rigs :

        Pelvis = Hips
        Left Hips = LeftUpLeg
        Left Knee = LeftLeg
        Left Foot = LeftFoot
        Left Arm = LeftArm
        LEfr Elbow = LeftForeArm
        Middle Spine = Spine
        Head = Head
    */
    public GameObject animatedModel;
    public GameObject RagdollModel;


    // Activate Ragdoll
    public void ToogleRagdool(bool state){
        switch(state){
            case true:
                animatedModel.SetActive(false);
                RagdollModel.SetActive(true);
            break;

            case false:
                animatedModel.SetActive(true);
                RagdollModel.SetActive(false);
            break;
        }
    }

    public void AddForceToRagdool(Vector3 directionAndForce){
        foreach (Rigidbody rigidbody in RagdollModel.GetComponentsInChildren<Rigidbody>())
        {
            rigidbody.AddForce(directionAndForce, ForceMode.Impulse);
        }
    }

}
