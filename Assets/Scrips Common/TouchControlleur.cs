﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using DG.Tweening;
using Lean.Touch;

public class TouchControlleur : MonoBehaviourSingleton<TouchControlleur>
{
    [Title("Lean Touch Variable")]
    public float LeanTouchstrenghSensibility = 1f;
    public LeanFingerFilter Use = new LeanFingerFilter(true);


    public Vector2 GetInputFromLeanTouch(){
        var fingers = Use.GetFingers();
        Vector2 screenDelta = LeanGesture.GetScaledDelta(fingers);

        return screenDelta * LeanTouchstrenghSensibility;
    }

}
