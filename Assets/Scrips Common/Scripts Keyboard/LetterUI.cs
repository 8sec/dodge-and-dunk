﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterUI : MonoBehaviour
{
    public string letter;
    private Image ownImage;
    private float alphaNormal;
    private Vector3 scaleInit;

    public bool NoScale = false;

    void Start()
    {
        ownImage = GetComponent<Image>();
        alphaNormal = ownImage.color.a;
        ownImage.color = new Color(0, 0, 0, 0);
        scaleInit = transform.localScale;
    }

    private int idColor, idScale;
    public void Press(bool rightLetter)
    {
        if (rightLetter)
        {
            ownImage.color = Color.green;
        }
        else
        {
            ownImage.color = Color.red;
        }

        ownImage.color = new Color(ownImage.color.r, ownImage.color.g, ownImage.color.b, 0);

        LeanTween.cancel(idColor);
        idColor = LeanTween.value(gameObject, ChangeColor, 0, alphaNormal, 0.2f).setLoopPingPong(1).id;

        if (!NoScale)
        {
            LeanTween.cancel(idScale);
            idScale = LeanTween.scale(gameObject, scaleInit + new Vector3(0.1f, 0.1f, 0.1f), 0.1f).setOnComplete(f =>
            {
                LeanTween.scale(gameObject, scaleInit, 0.1f);
            }).id;
        }
    }

    private void ChangeColor(float t)
    {
        ownImage.color = new Color(ownImage.color.r, ownImage.color.g, ownImage.color.b, t);
    }
}
