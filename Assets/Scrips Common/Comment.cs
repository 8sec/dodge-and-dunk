﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Comment : MonoBehaviour
{
    [SerializeField] [TextArea] private string comment;
}
