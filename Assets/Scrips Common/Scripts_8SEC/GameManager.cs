﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
using UnityEditor;
using AppsFlyerSDK;
using UnityEngine.SceneManagement;

public enum GameState
{
    INITIAL_STATE,
    IN_GAME,
    IN_GAME_REVIVED,
    GAME_OVER,
    LEVEL_SUCCESS,
    ASK_REVIVE
}
public class GameManager : MonoBehaviour
{
    public delegate void GameStartedDelegate();
    public delegate void GameEndedDelegate();
    public delegate void ScoreChangedDelegate(int n);
    public static event GameStartedDelegate OnGameStarted;
    public static event GameStartedDelegate OnGameEnded;
    public static event ScoreChangedDelegate OnScoreUpdate;


    static public GameManager Instance;

    public GameState CurrentGameState = GameState.INITIAL_STATE;
    public bool InGame
    {
        get
        {
            return CurrentGameState == GameState.IN_GAME;
        }
    }

    private int m_CurrentScore = 0;
    public int CurrentScore
    {
        get
        {
            return m_CurrentScore;
        }

        set
        {
            m_CurrentScore = value;
        }
    }

    protected virtual void Awake()
    {
        CurrentGameState = GameState.INITIAL_STATE;
        Time.timeScale = 1f;
        Application.targetFrameRate = 60;
        Instance = this;
    }


    // Update is called once per frame

    void Update()
    {

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
#endif
    }

    protected void TriggerGameEnded()
    {
        if (OnGameEnded != null)
            OnGameEnded();
    }

    public void StartSession()
    {
        print("STARTING THE SESSION");
        if (CurrentGameState == GameState.IN_GAME)
            return;
        CurrentGameState = GameState.IN_GAME;



        if (OnGameStarted != null)
            OnGameStarted();
        UIController.Instance.ShowPanel(UIPanelName.GAMEPLAY);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, LevelManager.Instance.CurrentLevelIndex.ToString("00"));

    }

    public void Revive()
    {
        // TODO : Revive Player
    }

    public void GameOver()
    {
        /*
        // If player has not been revived already
        if (CurrentGameState == GameState.IN_GAME)
        {
            // If ads are setup and rewarded is available :
            if (MonetizationManager.Instance != null && MonetizationManager.Instance.EnableMonetisation == true && MonetizationManager.Instance.isRewardedAdAvailable == true)
            {
                // we offer a revive instead of Game Over
                UIController.Instance.ShowPanel(UIPanelName.ASK_REVIVE);
                CurrentGameState = GameState.ASK_REVIVE;
                return;
            }

        }
        */
        
        // CLASSIC GAME OVER SITUATION
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, LevelManager.Instance.CurrentLevelIndex.ToString("000"));

        if (MonetizationManager.Instance != null)
        {
            MonetizationManager.Instance.ShowInterstitial();
        }

        UIController.Instance.ShowPanel(UIPanelName.GAME_OVER);
        CurrentGameState = GameState.GAME_OVER;
        VibrationManager.VibrateFailure();

        if (OnGameEnded != null)
            OnGameEnded();

        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
        if (LevelManager.Instance != null)
            richEvent.Add("af_levelIndex", (LevelManager.Instance.CurrentLevelIndex).ToString());
        AppsFlyer.sendEvent("af_Progress_LevelFailed", richEvent);

    }

    public void LevelSuccess()
    {

        UIController.Instance.ShowPanel(UIPanelName.LEVEL_SUCCESS);
        CurrentGameState = GameState.LEVEL_SUCCESS;
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, LevelManager.Instance.CurrentLevelIndex.ToString("000"));


        if (LevelManager.Instance != null)
        {
            LevelManager.Instance.LevelUp();
        }

        VibrationManager.VibrateSuccess();

        if (OnGameEnded != null)
            OnGameEnded();
    }

    public virtual void Retry()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);


    }

    public virtual void AddScore(int i)
    {
        m_CurrentScore += i;

        if (OnScoreUpdate != null)
            OnScoreUpdate(m_CurrentScore);

    }


}