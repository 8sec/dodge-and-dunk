﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectRepeater : MonoBehaviour
{

    public GameObject Target;
    
    public Vector3 CloneCount = Vector3.one;

    public Vector3 DistanceBetweenClones = Vector3.one;
    [ContextMenu("Clone Target")]
    public void CloneTarget()
    {
        if (Target == null)
        {
            Debug.LogError("Must Specify Target");
            return;
        }

        if(CloneCount.x < 1 || CloneCount.y <1 ||CloneCount.z <1)
        {
            Debug.LogError("Clone count must be at least 1 in X,Y,Z");
            return;
        }

            Vector3 xPos = Vector3.zero;
            Vector3 yPos = Vector3.zero;
            Vector3 zPos = Vector3.zero;
        for(int z = 0; z < CloneCount.z; z++)
        {
            zPos = z * Vector3.forward*DistanceBetweenClones.z;
            for (int x = 0; x < CloneCount.x; x++)
            {
                var go = Instantiate(Target, transform);
                
                xPos = DistanceBetweenClones.x* Vector3.right* x;

                go.transform.localPosition = xPos + zPos;
             

                for (int y = 1; y < CloneCount.y; y++)
                {

                    {
                        var go2 = Instantiate(Target, transform);
                        yPos = xPos + Vector3.up * y * DistanceBetweenClones.y;
                        go2.transform.localPosition = yPos + zPos;
                      

                    }

                }


            }
        }
        
    }




}
