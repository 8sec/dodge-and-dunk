﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
    public static UIController Instance;
    public RectTransform ScreenOffset;

    public float AnimationDuration = 0.25f;
    public LeanTweenType AnimationEase;
    public List<UIPanel> UIPanels = new List<UIPanel> ();
    public CanvasGroup CashOverlay;


    public Stack<UIPanel> screenHistory = new Stack<UIPanel>();


    public GameObject RewardedLoadingLockGo;

    // Use this for initialization
    void Awake () {
        Instance = this;
        if (FlashImage != null) {
            FlashImage.color = new Color (1f, 1F, 1F, 0f);

        }
        SetLoadingLockScreenState(false);
      //  ShowPanelInstantly(UIPanelName.SHOP);
       // HidePanelInstantly(UIPanelName.SHOP);
    }

    void Start ()
    {
        SetScreenBottomOffset(lastBottomOffsetPIXELS);
     //   if(UIPanels.Count > 0)
       //     ShowPanelInstantly (UIPanelName.TITLE_SCREEN,true);

    }

    // Update is called once per frame
    void Update () {

    
        if (scheduleUpdateOffset) {
            applyOffset ();
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.P))
            Debug_PrintScreenHistoryPeek();
#endif
    }

    public void ShowPanelInstantly (UIPanelName name) {
        ShowPanelInstantly (name, true);
    }

    int GetPanelIndex (UIPanelName name) {
        int n = -1;
        for (int i = 0; i < UIPanels.Count; i++) {
            if (UIPanels[i].PanelName == name) {
                n = i;
            }
        }
        return n;
    }

    public void ShowPanelInstantly (UIPanelName name, bool hideOtherPanels) {
        //    Debug.Log("Show Panel Instantly : " + n);


        if (name == UIPanelName.TREASURE_ROOM)
        {
            Debug.LogError("TRYING TO SHOW TREASURE ROOM");
        }

        int n = GetPanelIndex (name);
        if (n == -1) {
            Debug.LogError ("[UIController] Panel not found : " + name.ToString ());
            return;
        }

        UIPanels[n].gameObject.SetActive (true);
    
        if (hideOtherPanels) {
            for (int i = 0; i < UIPanels.Count; i++) {
                HidePanelInstantly (i);
            }
        }
        if (n >= 0 && n < UIPanels.Count) {
            if (screenHistory.Count == 0 || screenHistory.Peek() != UIPanels[n])// on ne rajoute pas l'écran dans l'historique s'il est déjà en haut de la stack (pour éviter d'avoir un doublon quand on fait showPreviousPanel
                screenHistory.Push(UIPanels[n]);
            UIPanels[n].ShowInstantly ();
        }
    }

    void Debug_PrintScreenHistoryPeek()
    {
        for (int i = 0; i < screenHistory.Count; i++)
        {
          var s=  screenHistory.Peek().name;
        }
    }

    public void ShowPreviousPanelInstantly()
    {
        if (screenHistory.Count >= 2)
        {
            screenHistory.Pop();

            ShowPanelInstantly(screenHistory.Peek().PanelName);
        }
        else
        {
            Debug.LogError("Not enough screens in history");
        }
    }

    public void ShowPreviousPanel()
    {
        if(screenHistory.Count >= 2)
        {
            screenHistory.Pop();
           
                ShowPanel(screenHistory.Peek().PanelName);
        }
        else
        {
            Debug.LogError("Not enough screens in history");
        }
    }

    public void ShowPanel (UIPanelName name) {
        ShowPanel (name, true);
    }

    public void ShowPanel(UIPanelName name, bool HideOtherPanels) {

        int n = GetPanelIndex(name);
        if (n == -1) {
            Debug.LogError("[UIController] Panel not found : " + name.ToString());
            return;
        }

        //  Debug.Log("Show Panel : " + n);
        UIPanels[n].gameObject.SetActive(true);
    
        if (HideOtherPanels) {
            for (int i = 0; i < UIPanels.Count; i++) {
                if (i != n)
                    HidePanel (i);
            }
        }
        if (n >= 0 && n < UIPanels.Count) {
            if(screenHistory.Count== 0 || screenHistory.Peek()!=UIPanels[n])// on ne rajoute pas l'écran dans l'historique s'il est déjà en haut de la stack (pour éviter d'avoir un doublon quand on fait showPreviousPanel
                screenHistory.Push(UIPanels[n]);
            UIPanels[n].Show(AnimationDuration);

        }
    }

    public void HidePanel (UIPanelName name) {
        int n = GetPanelIndex (name);
        if (n == -1) {
            Debug.LogError ("[UIController] Panel not found : " + name.ToString ());
            return;
        }
        HidePanel (n);
    }
    public void HidePanel (int i) {

        if (i >= 0 && i < UIPanels.Count) {
            UIPanels[i].Hide (AnimationDuration);

        }
    }

    public void HidePanelInstantly (UIPanelName name) {
        int n = GetPanelIndex (name);
        if (n == -1) {
            Debug.LogError ("[UIController] Panel not found : " + name.ToString ());
            return;
        }
        HidePanelInstantly (n);
    }
    void HidePanelInstantly (int n) {

        if (n >= 0 && n < UIPanels.Count) {
            UIPanels[n].HideInstantly ();
        }
    }

    public void ShowCashOverlay () {
        LeanTween.cancel (CashOverlay.gameObject);
        LeanTween.alphaCanvas (CashOverlay, 1f, AnimationDuration).setEase (AnimationEase).setIgnoreTimeScale (true);
    }

    public void HideCashOverlay () {
        LeanTween.cancel (CashOverlay.gameObject);
        LeanTween.alphaCanvas (CashOverlay, 0f, AnimationDuration).setEase (AnimationEase).setIgnoreTimeScale (true);
    }

    [Header ("Flash Settings")]
    public Image FlashImage;
    public float FlashDuration = 0.15f;
    public float FlashForce = 1f;
    public LeanTweenType FlashEase;
    private int ltFlash = -1;
    public void Flash () {
        if (FlashImage == null) {
            Debug.LogError ("Can't flash because FlashImage is null");
            return;
        }
        LeanTween.cancel (ltFlash);
        FlashImage.gameObject.SetActive (true);
        FlashImage.color = Color.white;

        ltFlash = LeanTween.value (gameObject, updateFlashAlpha, FlashForce, 0f, FlashDuration).setEase (FlashEase).id;
    }
    void updateFlashAlpha (float t) {
        Color c = Color.white;
        c.a = t;
        FlashImage.color = c;
    }

    static float lastBottomOffset = 0f;
    static float lastBottomOffsetPIXELS = 0f;
    bool scheduleUpdateOffset = false;
    public void SetScreenBottomOffset (float pixels) {
        if (ScreenOffset == null) {
            Debug.LogError ("You must fill a RectTransform as ScreenOffset");
            return;
        }
        scheduleUpdateOffset = true;
        lastBottomOffsetPIXELS = pixels;


        lastBottomOffset = (float)pixels / (float)Screen.height * GetComponent<CanvasScaler>().referenceResolution.y ;

    }

    void applyOffset () {
        if (ScreenOffset == null) {
            Debug.LogError (" SCREEN OFFSET NOT FOUND");
            return;
        }
        if (ScreenOffset != null)
        {
            var safeArea = Screen.safeArea;
            var canvas = GetComponent<Canvas>();
            var anchorMin = safeArea.position + new Vector2(0f, lastBottomOffset) ;
            var anchorMax = safeArea.position + safeArea.size ;
            anchorMin.x /= canvas.pixelRect.width;
            anchorMin.y /= canvas.pixelRect.height;
            anchorMax.x /= canvas.pixelRect.width;
            anchorMax.y /= canvas.pixelRect.height;

            ScreenOffset.anchorMin = anchorMin;
            // IGNORING TOP NOTCH in SAFE AREA
            ScreenOffset.anchorMax = anchorMax;

            // OLD VERRSION
            //ScreenOffset.offsetMin = new Vector2(ScreenOffset.offsetMin.x, lastBottomOffset);

        }
        scheduleUpdateOffset = false;
    }

    public void SetLoadingLockScreenState(bool locked)
    {

        if(RewardedLoadingLockGo !=null)
            RewardedLoadingLockGo.SetActive(locked);
    }
}