﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowGameObject : MonoBehaviour {

	private Vector3 offset;

	public bool Follow_Local = false;
	public bool LoockAtTarget = false;
	public bool lookAtTargetConstant = false;
	public Transform Target;
	public bool X;
	public bool Y;
	public bool Z;

	public bool OneWay_X;
	public bool OneWay_Y;
	public bool OneWay_Z;

	public bool FollowRotation;

	public bool Follow = true;
	public bool SmoothMove = false;
	private Vector3 camTarget;
	public float SmoothFollowMaxSpeed = 10f;
	public bool LimitX = false;
	public float LimitXFactor = 0.6f;

	void Awake () {
		if (Follow)
			StartFollow ();

		if(LoockAtTarget){
			transform.LookAt(Target);
		}
	}
	
    public void SetTarget(Transform target)
    {
        Target = target;
        StartFollow();
    }

	// Update is called once per frame
	void LateUpdate () {

        if (Target == null)
        {
            return;
        }
		if (!Follow)
			return;
		if (Follow_Local) {
			if (FollowRotation)
				transform.localRotation = Target.localRotation;
			Vector3 res = Target.localPosition - offset;
			if (LimitX)
				res.x *= LimitXFactor;
			if (!X || (OneWay_X && res.x < transform.localPosition.x))
				res.x = transform.localPosition.x;
			if (!Y || (OneWay_Y && res.y < transform.localPosition.y))
				res.y = transform.localPosition.y;
			if (!Z || (OneWay_Z && res.z < transform.localPosition.z))
				res.z = transform.localPosition.z;

			//transform.position = res;
			if (SmoothMove) {
				camTarget = res;
				transform.localPosition = Vector3.MoveTowards (transform.localPosition, camTarget, (SmoothFollowMaxSpeed * Time.deltaTime));

			} else {
				transform.localPosition = res;

			}
		} else {
			if (FollowRotation)
				transform.rotation = Target.rotation;
			Vector3 res = Target.position - offset;
			if (LimitX)
				res.x *= LimitXFactor;
			if (LimitX)
				res.x *= LimitXFactor;
			if (!X || (OneWay_X && res.x < transform.position.x))
				res.x = transform.position.x;
			if (!Y || (OneWay_Y && res.y < transform.position.y))
				res.y = transform.position.y;
			if (!Z || (OneWay_Z && res.z < transform.position.z))
				res.z = transform.position.z;
			//transform.position = res;
			if (SmoothMove) {
				camTarget = res;
				transform.position = Vector3.MoveTowards (transform.position, camTarget, (SmoothFollowMaxSpeed * Time.deltaTime));

			} else {
				transform.position = res;

			}
		}

		if(lookAtTargetConstant){
			transform.LookAt(Target);
		}

	}

	public void StartFollow () {

        if (Target != null)
        {
            offset = Target.position - transform.position;
            Follow = true;
            enabled = true;
        }
	
	}

	public void StopFollow () {
		Follow = false;
	}
}