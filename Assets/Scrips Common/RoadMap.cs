﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

public class RoadMap : MonoBehaviour
{
    public TextMeshProUGUI currentLevel;
    public TextMeshProUGUI nextLevel;
    public Image progressionSlider;

    [Button("UpdateRoadMap")]    
    public void UpdateRoadMap(){
        float progressionTremollo = (LevelManager.Instance.CurrentLevelIndex % 5);
        float numberLeft = LevelManager.Instance.CurrentLevelIndex - progressionTremollo;

        currentLevel.text = (numberLeft).ToString();
        nextLevel.text = (numberLeft + 5).ToString();

        progressionSlider.fillAmount = progressionTremollo.Remap(0f, 5f, 0f, 1f);
    }
}
