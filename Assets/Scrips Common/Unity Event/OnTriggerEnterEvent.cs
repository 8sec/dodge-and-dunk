﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTriggerEnterEvent : MonoBehaviour
{
    public string detectionTag;
    public UnityEvent onTriggerEnter;

    private void OnTriggerEnter(Collider other) {
        if(detectionTag != string.Empty && other.gameObject.tag == detectionTag){
            onTriggerEnter.Invoke();
        }

        if(detectionTag == string.Empty){
            onTriggerEnter.Invoke();
        }
    }
}
