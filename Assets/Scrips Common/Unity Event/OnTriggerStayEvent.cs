﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTriggerStayEvent : MonoBehaviour
{
    public string detectionTag;
    public UnityEvent onTriggerStay;

    private void OnTriggerStay(Collider other) {
        if(detectionTag != string.Empty && other.gameObject.tag == detectionTag){
            onTriggerStay.Invoke();
        }

        if(detectionTag == string.Empty){
            onTriggerStay.Invoke();
        }
    }
}
