﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LevelManager))]
[ExecuteInEditMode]
public class ReadCurentLevelIndex : MonoBehaviour
{
    LevelManager levelManager;
    private const string Key_CurrentLevel = "LM_CurrentLevel";
    public void Start() {
        levelManager = GetComponent<LevelManager>();
        levelManager.CurrentLevelIndex = PlayerPrefs.GetInt(Key_CurrentLevel, 1);
    }
}
