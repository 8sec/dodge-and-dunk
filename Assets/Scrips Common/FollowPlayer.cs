﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;
    public bool followY = false;
    public bool LookAtTarget = false;



    void Update()
    {
        // Define a target position above and behind the target transform
        Vector3 targetPosition = target.TransformPoint(offset);

        // Smoothly move the camera towards that target position
        if(followY == false){
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition , ref velocity, smoothTime).SetY(transform.position.y);
        }else{
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition , ref velocity, smoothTime);
        }

        if(LookAtTarget){
            Quaternion lookRotation = Quaternion.LookRotation(target.position - transform.position);
            transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, 10f * Time.deltaTime);
        }
    }
}
