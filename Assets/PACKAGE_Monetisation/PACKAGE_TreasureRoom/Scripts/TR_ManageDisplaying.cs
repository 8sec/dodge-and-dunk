﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TR_ManageDisplaying : MonoBehaviour
{
    public static TR_ManageDisplaying Instance;
    [SerializeField] private GameObject _goToChestRoomButton;
    [SerializeField] private GameObject _rewardedButtonChestRoom;
    [SerializeField] private GameObject _noThanksRewardedChestRoom;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
   
    public void EnableChestRoomRewarded()
    {
        _goToChestRoomButton.SetActive(false);
        _rewardedButtonChestRoom.SetActive(true);
        _noThanksRewardedChestRoom.SetActive(true);
    }
    public void EnableGoToChestRoomButton()
    {
        _goToChestRoomButton.SetActive(true);
        _rewardedButtonChestRoom.SetActive(false);
        _noThanksRewardedChestRoom.SetActive(false);
    }
}
