﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;


public class TR_KeyManager : MonoBehaviourSingleton<TR_KeyManager>
{

    #region Event
    public delegate void OnKeyUsed();
    public static event OnKeyUsed OnKeyUsedEvent;

    public delegate void OnChestRoomOpened();
    public static event OnChestRoomOpened OnChestRoomOpenedEvent;

    public delegate void OnChestRoomClosed();
    public static event OnChestRoomClosed OnChestRoomClosedEvent;
    #endregion

    #region Fields
    [SerializeField] private int _maxKeyNumber;
    #endregion

    #region Getters
    public int MaxKeyNumber => _maxKeyNumber;
    private int _currentKeyNumber;
    public int CurrentKeyNumber => _currentKeyNumber;

    public bool HasKeyLeft { get { return _currentKeyNumber > 0; } }

    public bool canOpenChestRoom { get { return CurrentKeyNumber >= MaxKeyNumber; } }

    #endregion

    private const string CURRENT_TREASURE_KEY_NUMBER_KEY = "CurrentTreasureKeyNumber";

    private void Start()
    {
        _currentKeyNumber = PlayerPrefs.GetInt(CURRENT_TREASURE_KEY_NUMBER_KEY, 0);
    }

    [Button("Add Key")]
    public void AddKey(int keysToAdd)
    {
        if (_currentKeyNumber < _maxKeyNumber)
        {
            _currentKeyNumber += keysToAdd;

            if (_currentKeyNumber > _maxKeyNumber)
            {
                _currentKeyNumber = _maxKeyNumber;
            }

            OnKeyUsedEvent?.Invoke();
        }

        else
        {
            Debug.LogError("Can't add more key : current key number would be higher than max key number");
        }
    }

    public void SaveKeyPrefab()
    {
        PlayerPrefs.SetInt(CURRENT_TREASURE_KEY_NUMBER_KEY, _currentKeyNumber);
    }

    public void UseKey()
    {
        if (_currentKeyNumber > 0)
        {
            _currentKeyNumber--;
            OnKeyUsedEvent?.Invoke();
            PlayerPrefs.SetInt(CURRENT_TREASURE_KEY_NUMBER_KEY, _currentKeyNumber);
        }

        else
        {
            Debug.LogError("Can't use any key : current key number = 0");
        }
    }

    public void OpenChestRoom()
    {
        UIController.Instance.ShowPanelInstantly(UIPanelName.TREASURE_ROOM, true);
        OnChestRoomOpenedEvent?.Invoke();
        AddKey(3);
    }

    public void CloseChestRoom()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }
    
}
