﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TR_NoThanksButton : MonoBehaviour
{
    public void Action()
    {
        TR_KeyManager.Instance.CloseChestRoom();
    }
}
