﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TR_RoadToChestRoomOverlay : MonoBehaviour
{
    [SerializeField] private Color _roadStepDisabledColor;
    [SerializeField] private Color _roadStepEnabledColor;

    [SerializeField] private Image[] _roadStepsFill;
    [SerializeField] private Image[] _trophysEnabled;

    [SerializeField] private Image _finalStepFill;

    private void Awake()
    {
        TR_KeyManager.OnKeyUsedEvent += UpdateDisplayToCurrentKeyAmount;
        TR_KeyManager.OnChestRoomOpenedEvent += Hide;
        ShopManager.OnShopOpenedEvent += Hide;
        ShopManager.OnShopExitEvent += Show;
    }

    private void OnDestroy()
    {
        TR_KeyManager.OnKeyUsedEvent -= UpdateDisplayToCurrentKeyAmount;
        TR_KeyManager.OnChestRoomOpenedEvent -= Hide;
        ShopManager.OnShopOpenedEvent -= Hide;
        ShopManager.OnShopExitEvent -= Show;
    }

    private void Start()
    {
        if (true)//GameManager_RJ.Instance.CurrentLevel > 0)
        {
            gameObject.SetActive(true);
            UpdateDisplay(TR_KeyManager.Instance.CurrentKeyNumber);
        }

        else
        {
            gameObject.SetActive(false);
        }
    }

    public void UpdateDisplay(int keyAmount)
    {
        for (int i = 0; i < _roadStepsFill.Length; i++)
        {
            if (i < keyAmount)
            {
                // Activated

                _roadStepsFill[i].color = _roadStepEnabledColor;
                _trophysEnabled[i].enabled = true;

                if (i == keyAmount - 1)
                {
                    GameObject g = _trophysEnabled[i].gameObject;
                    g.transform.localScale = Vector3.one * 3;
                    LeanTween.scale(g, Vector3.one, 0.25f).setEase(LeanTweenType.easeOutBack);
                }
              
             
            }

            else
            {
                _roadStepsFill[i].color = _roadStepDisabledColor;
                _trophysEnabled[i].enabled = false;
            }
        }

        if (keyAmount == 3)
        {
            _finalStepFill.color = _roadStepEnabledColor;
        }

        else
        {
            _finalStepFill.color = _roadStepDisabledColor;
        }
    }

    public void UpdateDisplayToCurrentKeyAmount()
    {
        UpdateDisplay(TR_KeyManager.Instance.CurrentKeyNumber);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }




}
