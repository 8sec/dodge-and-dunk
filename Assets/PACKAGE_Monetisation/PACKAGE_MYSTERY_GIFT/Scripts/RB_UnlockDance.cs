﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RB_UnlockDance : RewardedButton
{
    public override void CollectReward()
    {
        Debug.LogError("BUTTON CLICKED");
        base.CollectReward();
        MysteryManager.Instance.UnlockMysteryGift();
    }
}
