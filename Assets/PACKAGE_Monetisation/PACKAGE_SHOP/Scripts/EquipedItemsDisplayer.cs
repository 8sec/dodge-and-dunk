﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipedItemsDisplayer : MonoBehaviour
{
    [SerializeField] private MeshRenderer _meshRenderer;
    [SerializeField] private List<SpawnableDisplayer> _spawnableDisplayers = new List<SpawnableDisplayer>();
    [SerializeField] private Animator _animator;
    private string _animationWinTrigger;
    // Start is called before the first frame update

    private void Awake()
    {
        ShopManager.OnShopUpdateEvent += UpdatePlayerSkin;
        ItemCategory.OnCategoryInitializedEvent += UpdatePlayerSkin;
    }

    private void OnDestroy()
    {
        ShopManager.OnShopUpdateEvent -= UpdatePlayerSkin;
        ItemCategory.OnCategoryInitializedEvent -= UpdatePlayerSkin;
    }

    private void UpdatePlayerSkin()
    {
        ShopItem_Color ColorItem = (ShopItem_Color)ShopManager.Instance.GetCurrentlySelectedItem(ShopCategoryItem.COLOR);

        if (ColorItem != null)
        {
            _meshRenderer.material.color = ColorItem.Color;
        }

        ShopItem_Animation AnimationItem = (ShopItem_Animation)ShopManager.Instance.GetCurrentlySelectedItem(ShopCategoryItem.ANIMATION);

        if (AnimationItem != null)
        {
            _animationWinTrigger = AnimationItem.AnimationName;
            _animator.SetTrigger(_animationWinTrigger);
        }

        for (int i = 0; i < _spawnableDisplayers.Count; i++)
        {
            _spawnableDisplayers[i].UpdateDisplay();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            ShopManager.Instance.OpenShop();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            TR_KeyManager.Instance.OpenChestRoom();
        }
    }

}
