﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Text))]
public class ShopProgressTracker : MonoBehaviour
{

    private Text m_Text;
    private void Awake()
    {
        m_Text = GetComponent<Text>();

    }
    // Start is called before the first frame update
    void Start()
    {
        UpdateCounter();
        ShopManager.OnItemUnlocked += updateCounterEvent;
    }

    private void OnDestroy()
    {
        ShopManager.OnItemUnlocked -= updateCounterEvent;

    }

    private void updateCounterEvent(ShopItem unlockedItem)
    {
        UpdateCounter();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

void UpdateCounter()
    {
        m_Text.text = ShopManager.Instance.UnlockedItems.ToString("00") + "/" + ShopManager.Instance.AvailableItems.Count.ToString("00");
    }
}
