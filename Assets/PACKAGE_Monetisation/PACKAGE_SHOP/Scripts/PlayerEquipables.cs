﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipables : MonoBehaviour
{
    public static PlayerEquipables Instance;

    [SerializeField] private Transform _playerVisualParent;

    public Transform PlayerVisualParent => _playerVisualParent;

    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Replace current player visual by "equipableObject"
    /// </summary>
    /// <param name="equipableObject"></param>
    public void SetEquipable(GameObject equipableObject)
    {

        foreach (Transform t in _playerVisualParent)
        {
            Destroy(t.gameObject);
        }

        var neo = Instantiate(equipableObject, _playerVisualParent);
        neo.SetActive(true);
        neo.transform.localPosition = Vector3.zero;
        neo.transform.localScale = Vector3.one * 1;
    }


}
