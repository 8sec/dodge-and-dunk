﻿
[System.Serializable]
public struct DictionaryToJson 
{
    public ShopCategoryItem ShopCategoryItem;
    public string SavedItemID;

    public DictionaryToJson(ShopCategoryItem shopCategoryItem, string savedItemID)
    {
        ShopCategoryItem = shopCategoryItem;
        SavedItemID = savedItemID;
    }
   
}
