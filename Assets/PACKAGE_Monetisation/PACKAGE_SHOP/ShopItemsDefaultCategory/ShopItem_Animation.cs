﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopItem_Animation : ShopItem
{
    [SerializeField] private string _animationName;
    public string AnimationName => _animationName;
}
