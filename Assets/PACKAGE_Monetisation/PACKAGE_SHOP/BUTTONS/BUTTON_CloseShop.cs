﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BUTTON_CloseShop : MonoBehaviour
{
    public void Action()
    {
        if (UIController.Instance == null)
        {
            Debug.LogError("UIController instance is NULL. Please setup a UIController in the scene");
            return;
        }

        // TODO REMETTRE LE PANEL TITLE SCREEN A LA PLACE DE GAMEPLAY
        UIController.Instance.ShowPanelInstantly(UIPanelName.GAMEPLAY, true);
        ShopManager.Instance.OnShopExitButton();
    }
}
