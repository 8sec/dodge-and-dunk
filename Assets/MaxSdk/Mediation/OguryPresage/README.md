# OguryPresage

### Warning: This Unity adapter must be pushed manually.
UI, the Bintray package, and the Unity package all use `ogury-presage` with a dash.
This is the only Unity package with a dash in the name, which the build and push scripts do not support.
