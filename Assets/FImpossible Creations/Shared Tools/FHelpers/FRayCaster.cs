﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// FImpossible Creations: https://www.youtube.com/c/FImpossibleCreations
/// Class for visualizing and using inside code raycasting with line, box, sphere or capsule shape
/// You can add it to any transform, control when raycast should be done with caster.UpdateCast() method
/// If you just need one RaycastHit you can call CastAndGet() instead
/// If you need data of all collided objects, increase 'MaxContacts' and access AllHits array
/// Note that any scale capsule cast costs similar cpu as 3 raycasts, sphere/box cast similar cpu as 2 raycasts
/// </summary>
public class FRayCaster : MonoBehaviour
{

    [FPD_Header("Physics Cast Setup", 4, 6)]
    public EFCastType CastType = EFCastType.BoxCast;
    public QueryTriggerInteraction CollideWithTriggers = QueryTriggerInteraction.Ignore;
    public LayerMask CollisionMask = 1 << 0;
    [Tooltip("If detection should not stop in first collision but go further resulting in multiple RaycastHit data stored in AllHits[] array")]
    public int MaxContacts = 1;


    [FPD_Header("Define Cast Space", 8, 6)]
    public Vector3 PositionOffset = Vector3.zero;
    [Tooltip("What transform raycasting should follow for position and scale, set null to use 'PositionOffset' as world position")]
    public Transform PosScaleSpace;
    [Space(4)]
    public Vector3 RotationOffset = Vector3.zero;
    /// <summary> Can be set through code to avoid euler conversions, 'RotationOffset' must be set to Vector3.zero to make this variable work </summary>
    internal Quaternion rotationOffset = Quaternion.identity;
    [Tooltip("What transform raycasting should follow for rotation, set null to use 'RotationOffset' as rotation")]
    public Transform RotationSpace;


    [FPD_Header("Casting Variables", 8, 6)]
    public float MaxDistance = 1f;
    public Vector3 Direction = Vector3.forward;
    public Vector3 HalfExtends = new Vector3(1f, 1f, 0.1f);
    public float Radius = 1f;


    [FPD_Header("Debug Visualization", 8, 6)]
    [SerializeField] private bool drawFinish = true;
    [SerializeField] private bool drawHitNormals = false;


    /// <summary> If not using cast All then there is stored raycast result </summary>
    public RaycastHit Hit;
    /// <summary> If using Cast All then all hits are stored in this array, use 'AllHitsCount' variable to not search through whole array on preallocated null data </summary>
    public RaycastHit[] AllHits;
    /// <summary> Count of last raycast hit collision amount, helpful when using 'MaxContacts' feature </summary>
    internal int AllHitsCount = 0;



    // > > > PUBLIC USER METHODS BELOW --------------------------------------------


    /// <summary>
    /// Casting ray with choosed settings and returning RaycastHit data
    /// If you just need to check if something was hitted do caster.CastAndGet().transform != null -> Collided with something!
    /// </summary>
    public RaycastHit User_CastAndGet()
    {
        User_UpdateCast();
        return User_GetLastHit();
    }


    /// <summary>
    /// Casting ray with choosed settings
    /// Data will be stored in caster.Hit variable or caster.AllHits if using MaxContacts greater than 1
    /// </summary>
    public void User_UpdateCast()
    {
        switch (CastType)
        {
            case EFCastType.RayCast: RayCast(); break;
            case EFCastType.BoxCast: BoxCast(); break;
            case EFCastType.SphereCast: SphereCast(); break;
            case EFCastType.CapsuleCast: CapsuleCast(); break;
        }
    }


    /// <summary>
    /// If last UpdateCast() collided with any collider
    /// (method is not calling UpdateCast() it must be done before calling it)
    /// </summary>
    public bool User_WasHitting()
    {
        return AllHitsCount > 0;
    }


    /// <summary>
    /// Getting last first hit (even when using MaxContacts > 1)
    /// (method is not calling UpdateCast() it must be done before calling it)
    /// </summary>
    public RaycastHit User_GetLastHit()
    {
        if (MaxContacts == 1) return Hit; else return AllHits[0];
    }


    /// <summary>
    /// Getting array of hit data for last hitted objects
    /// I suggest using caster.AllHits for loop with caster.AllHitsCount to avoid array memory allocation which can cause garbage collector's peaks
    /// </summary>
    private RaycastHit[] User_GetLastHits()
    {
        if (MaxContacts == 1)
            return new RaycastHit[1] { Hit };
        else
        {
            RaycastHit[] hits = new RaycastHit[AllHitsCount];
            for (int i = 0; i < AllHitsCount; i++) hits[i] = AllHits[i];
            return hits;
        }
    }



    // < < < PUBLIC USER METHODS END --------------------------------------------




    #region Validation


    private void Reset()
    {
        PosScaleSpace = transform;
        RotationSpace = transform;
    }

    private void OnValidate()
    {
        if (MaxContacts < 1) MaxContacts = 1;
        if (MaxContacts > 1000) MaxContacts = 1000;
    }


    #endregion


    #region Computing raycasting parameters


    internal Vector3 GetCastOrigin()
    {
        if (PosScaleSpace != null)
            return PosScaleSpace.position + PosScaleSpace.TransformVector(PositionOffset);
        else
            return PositionOffset;
    }


    internal Vector3 GetCastScale()
    {
        if (PosScaleSpace)
            return Vector3.Scale(HalfExtends, PosScaleSpace.lossyScale);
        else
            return HalfExtends;
    }


    internal Vector3 GetCastDirection()
    {
        if (RotationSpace)
            return ((RotationSpace.rotation * GetRotation()) * Direction).normalized;
        else
            return (GetRotation() * Direction).normalized;
    }


    internal Quaternion GetCastRotation()
    {
        if (RotationSpace)
            return RotationSpace.rotation * GetRotation();
        else
            return GetRotation();
    }


    internal float GetCastDistance()
    {
        if (PosScaleSpace)
        {
            return PosScaleSpace.TransformVector(PosScaleSpace.InverseTransformDirection(GetCastDirection().normalized)).magnitude * MaxDistance; // maxDistance forward scalled with object
        }
        else
            return MaxDistance;
    }


    private float GetScaler()
    {
        if (PosScaleSpace == null)
            return 1f;
        else
        {
            if (PosScaleSpace.lossyScale.x > PosScaleSpace.lossyScale.y)
            {
                if (PosScaleSpace.lossyScale.x > PosScaleSpace.lossyScale.z)
                    return PosScaleSpace.lossyScale.x;
                else
                    return PosScaleSpace.lossyScale.z;
            }
            else // x smaller than y
            if (PosScaleSpace.lossyScale.z > PosScaleSpace.lossyScale.y)
                return PosScaleSpace.lossyScale.z;
            else
                return PosScaleSpace.lossyScale.y;
        }
    }

    private Quaternion GetRotation()
    {
        if (RotationOffset.x == 0 && RotationOffset.y == 0 && RotationOffset.z == 0)
            return rotationOffset;
        else
            return Quaternion.Euler(RotationOffset);
    }


    #endregion


    #region Additional Casting Computations


    private void RefreshAllCast()
    {
        if (AllHits == null) AllHits = new RaycastHit[MaxContacts];
        else
        if (AllHits.Length != MaxContacts) AllHits = new RaycastHit[MaxContacts];
    }


    #endregion



    /// <summary>
    /// Triggering visualization of casting only when object is selected in editor
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        switch (CastType)
        {
            case EFCastType.RayCast: Gizmos_RayCast(); break;
            case EFCastType.BoxCast: Gizmos_BoxCast(); break;
            case EFCastType.SphereCast: Gizmos_SphereCast(); break;
            case EFCastType.CapsuleCast: Gizmos_CapsuleCast(); break;
        }

        Gizmos.matrix = Matrix4x4.identity;

        if (drawHitNormals)
            if (User_WasHitting())
            {
                Gizmos.color = new Color(0.5f, 0.5f, 0f, 0.7f);

                if (MaxContacts > 1)
                    for (int i = 0; i < AllHits.Length; i++) { if (AllHits[i].transform == null) break; Gizmos.DrawRay(AllHits[i].point, AllHits[i].normal); }
                else
                    Gizmos.DrawRay(Hit.point, Hit.normal);
            }

        //DiagnozeCastTime();
    }


    #region Measuring performance

    //public float debugTime = 0f;
    //private void DiagnozeCastTime()
    //{
    //    System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
    //    watch.Start();

    //    switch (CastType)
    //    {
    //        case EFCastType.RayCast: for (int i = 0; i < 1000; ++i) { RayCast(); } break;
    //        case EFCastType.BoxCast: for (int i = 0; i < 1000; ++i) { BoxCast(); } break;
    //        case EFCastType.SphereCast: for (int i = 0; i < 1000; ++i) { SphereCast(); } break;
    //        case EFCastType.CapsuleCast: for (int i = 0; i < 1000; ++i) { CapsuleCast(); } break;
    //    }

    //    watch.Stop();
    //    debugTime = watch.ElapsedTicks;
    //}

    #endregion


    #region RayCast


    private void RayCast()
    {
        if (MaxContacts == 1)
        {
            if (Physics.Raycast(GetCastOrigin(), GetCastDirection(), out Hit, GetCastDistance(), CollisionMask, CollideWithTriggers)) AllHitsCount = 1; else AllHitsCount = 0;
        }
        else
        {
            RefreshAllCast();
            AllHitsCount = Physics.RaycastNonAlloc(GetCastOrigin(), GetCastDirection(), AllHits, GetCastDistance(), CollisionMask, CollideWithTriggers);
        }
    }



    private void Gizmos_RayCast()
    {
        Gizmos.matrix = Matrix4x4.identity;

        RayCast();

        if (User_WasHitting())
            Gizmos.color = new Color(1f, 0.2f, 0.5f, 0.45f);
        else
            Gizmos.color = new Color(.1f, 1f, 0.5f, 0.5f);

        Gizmos.DrawRay(GetCastOrigin(), GetCastDirection() * GetCastDistance());

        if (drawFinish) Gizmos.DrawWireSphere(GetCastOrigin() + GetCastDirection() * GetCastDistance(), GetCastDistance() * 0.004f);
    }


    #endregion


    #region BoxCast

    private void BoxCast()
    {
        if (MaxContacts == 1)
        {
            if (Physics.BoxCast(GetCastOrigin(), GetCastScale(), GetCastDirection(), out Hit, GetCastRotation(), GetCastDistance(), CollisionMask, CollideWithTriggers)) AllHitsCount = 1; else AllHitsCount = 0;
        }
        else
        {
            RefreshAllCast();
            AllHitsCount = Physics.BoxCastNonAlloc(GetCastOrigin(), GetCastScale(), GetCastDirection(), AllHits, GetCastRotation(), GetCastDistance(), CollisionMask, CollideWithTriggers);
        }
    }

    private void Gizmos_BoxCast()
    {

        BoxCast();

        if (User_WasHitting())
            Gizmos.color = new Color(1f, 0.2f, 0.5f, 0.45f);
        else
            Gizmos.color = new Color(.1f, 1f, 0.5f, 0.5f);

        Gizmos.matrix = Gizmos_GetMatrix();
        Gizmos.DrawWireCube(Vector3.zero, HalfExtends * 2f);

        if (drawFinish)
        {
            Gizmos.color = new Color(1f, 1f, 1f, 0.1f);
            Gizmos.DrawWireCube(Direction.normalized * MaxDistance, HalfExtends * 2f);

            if (User_WasHitting())
                Gizmos.color = new Color(1f, 0.2f, 0.5f, 0.45f);
            else
                Gizmos.color = new Color(.1f, 1f, 0.5f, 0.5f);

            Gizmos.DrawRay(Vector3.up * HalfExtends.y + Vector3.right * HalfExtends.x, Direction.normalized * MaxDistance);
            Gizmos.DrawRay(Vector3.up * HalfExtends.y - Vector3.right * HalfExtends.x, Direction.normalized * MaxDistance);

            Gizmos.DrawRay(-Vector3.up * HalfExtends.y + Vector3.right * HalfExtends.x, Direction.normalized * MaxDistance);
            Gizmos.DrawRay(-Vector3.up * HalfExtends.y - Vector3.right * HalfExtends.x, Direction.normalized * MaxDistance);
        }
        else
            Gizmos.DrawRay(Vector3.zero, Direction.normalized * MaxDistance);

    }

    #endregion


    #region SphereCast

    private void SphereCast()
    {
        if (MaxContacts == 1)
        {
            if (Physics.SphereCast(GetCastOrigin(), Radius * GetScaler(), GetCastRotation() * Direction, out Hit, GetCastDistance(), CollisionMask, CollideWithTriggers)) AllHitsCount = 1; else AllHitsCount = 0;
        }
        else
        {
            RefreshAllCast();
            AllHitsCount = Physics.SphereCastNonAlloc(GetCastOrigin(), Radius * GetScaler(), GetCastRotation() * Direction, AllHits, GetCastDistance(), CollisionMask, CollideWithTriggers);
        }
    }

    private void Gizmos_SphereCast()
    {
        SphereCast();

        if (User_WasHitting())
            Gizmos.color = new Color(1f, 0.2f, 0.5f, 0.45f);
        else
            Gizmos.color = new Color(.1f, 1f, 0.5f, 0.5f);

        Gizmos.matrix = Gizmos_GetMatrix();

        float radius = Radius;
        Gizmos.DrawWireSphere(Vector3.zero, radius);

        if (drawFinish)
        {
            Gizmos.DrawRay(Vector3.up * radius, Direction * MaxDistance);
            Gizmos.DrawRay(Vector3.right * radius, Direction * MaxDistance);
            Gizmos.DrawRay(Vector3.down * radius, Direction * MaxDistance);
            Gizmos.DrawRay(-Vector3.right * radius, Direction * MaxDistance);

            Gizmos.color = new Color(1f, 1f, 1f, 0.1f);
            Gizmos.DrawWireSphere(Direction * MaxDistance, Radius);
        }
        else
            Gizmos.DrawRay(Vector3.zero, GetCastDirection() * MaxDistance);
    }

    #endregion


    #region CapsuleCast

    private Vector3 GetUpCapsulePointOffset()
    {
        if (RotationSpace == null)
            return GetRotation() * Vector3.up * HalfExtends.y;
        else
            return (RotationSpace.rotation * GetRotation()) * Vector3.up * HalfExtends.y;
    }



    private void CapsuleCast()
    {
        Vector3 origin = GetCastOrigin();

        Vector3 upPoint = GetUpCapsulePointOffset();
        if (PosScaleSpace) upPoint = Vector3.Scale(PosScaleSpace.lossyScale, upPoint);

        if (MaxContacts == 1)
        {

            if (Physics.CapsuleCast(origin + upPoint, origin - upPoint, Radius * GetScaler(), GetCastDirection(), out Hit, GetCastDistance(), CollisionMask, CollideWithTriggers)) AllHitsCount = 1; else AllHitsCount = 0;
        }
        else
        {
            RefreshAllCast();
            AllHitsCount = Physics.CapsuleCastNonAlloc(origin + upPoint, origin - upPoint, Radius * GetScaler(), GetCastDirection(), AllHits, GetCastDistance(), CollisionMask, CollideWithTriggers);
        }
    }

    private void Gizmos_CapsuleCast()
    {
        CapsuleCast();

        if (User_WasHitting())
            Gizmos.color = new Color(1f, 0.2f, 0.5f, 0.45f);
        else
            Gizmos.color = new Color(.1f, 1f, 0.5f, 0.5f);

        Gizmos.matrix = Gizmos_GetMatrix();
        float radius = Radius;

        Vector3 upOff = Vector3.up * HalfExtends.y;

        Gizmos.DrawWireSphere(upOff, radius);
        Gizmos.DrawWireSphere(-upOff, radius);

        Gizmos.DrawRay(upOff + Vector3.right * radius, Vector3.down * HalfExtends.y * 2f);
        Gizmos.DrawRay(upOff - Vector3.right * radius, Vector3.down * HalfExtends.y * 2f);
        //Gizmos.DrawRay(upOff + Vector3.forward * radius, Vector3.down * HalfExtends.y * 2f);
        Gizmos.DrawRay(upOff - Vector3.forward * radius, Vector3.down * HalfExtends.y * 2f);


        if (drawFinish)
        {
            Vector3 finOrigin = Direction * MaxDistance;
            Gizmos.DrawRay(upOff + Vector3.right * radius + Vector3.down * HalfExtends.y * 2f, Direction * MaxDistance);
            Gizmos.DrawRay(upOff - Vector3.right * radius + Vector3.down * HalfExtends.y * 2f, Direction * MaxDistance);
            Gizmos.DrawRay(upOff - Vector3.up * radius + Vector3.down * HalfExtends.y * 2f, Direction * MaxDistance);

            Gizmos.DrawRay(-upOff + Vector3.right * radius + Vector3.up * HalfExtends.y * 2f, Direction * MaxDistance);
            Gizmos.DrawRay(-upOff - Vector3.right * radius + Vector3.up * HalfExtends.y * 2f, Direction * MaxDistance);
            Gizmos.DrawRay(-upOff + Vector3.up * radius + Vector3.up * HalfExtends.y * 2f, Direction * MaxDistance);


            Gizmos.color = new Color(1f, 1f, 1f, 0.1f);
            Gizmos.DrawWireSphere(finOrigin + upOff, radius);
            Gizmos.DrawWireSphere(finOrigin - upOff, radius);
            Gizmos.DrawRay(finOrigin + upOff + Vector3.forward * radius, Vector3.down * HalfExtends.y * 2f);
            Gizmos.DrawRay(finOrigin + upOff + Vector3.right * radius, Vector3.down * HalfExtends.y * 2f);
            Gizmos.DrawRay(finOrigin + upOff - Vector3.right * radius, Vector3.down * HalfExtends.y * 2f);
            //Gizmos.DrawRay(finOrigin + upOff - Vector3.forward * radius, Vector3.down * HalfExtends.y * 2f);
        }
        else
            Gizmos.DrawRay(Vector3.zero, Direction * MaxDistance);

    }


    #endregion



    #region Enums and others


    public enum EFCastType { RayCast, BoxCast, SphereCast, CapsuleCast }

    Matrix4x4 Gizmos_GetMatrix()
    {
        if (PosScaleSpace != null)
        {
            if (RotationSpace != null)
                return Matrix4x4.TRS(PosScaleSpace.position + PositionOffset, RotationSpace.rotation * GetRotation(), PosScaleSpace.lossyScale);
            else
                return Matrix4x4.TRS(PosScaleSpace.position + PositionOffset, GetRotation(), PosScaleSpace.lossyScale);
        }
        else
        {
            return Matrix4x4.TRS(PositionOffset, RotationSpace == null ? GetRotation() : RotationSpace.rotation, Vector3.one);
        }
    }


    #endregion



    #region Dynamic Editor Window

#if UNITY_EDITOR
    /// <summary>
    /// FM: Editor class component to enchance controll over component from inspector window
    /// </summary>
    [UnityEditor.CustomEditor(typeof(FRayCaster))]
    [UnityEditor.CanEditMultipleObjects]
    public class FRayCasterEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            FRayCaster get = (FRayCaster)target;

            List<string> ignored = new List<string>();
            ignored.Add("m_Script");

            if (get.CastType == EFCastType.RayCast)
            { ignored.Add("HalfExtends"); ignored.Add("Radius"); }
            else if (get.CastType == EFCastType.BoxCast)
                ignored.Add("Radius");
            else
                if (get.CastType == EFCastType.SphereCast)
                ignored.Add("HalfExtends");
            else
                UnityEditor.EditorGUILayout.HelpBox("Use HalfExtends.y for capsule size", UnityEditor.MessageType.None);

            DrawPropertiesExcluding(serializedObject, ignored.ToArray());
            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();
        }
    }
#endif

    #endregion


}
