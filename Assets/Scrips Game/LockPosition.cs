﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockPosition : MonoBehaviour
{
    public bool lockY;

    private Vector3 originalPosition;
    private void Awake() {
        originalPosition = transform.position;
    }

    private void LateUpdate() {
        transform.position = transform.position.SetY(originalPosition.y);
    }
}
