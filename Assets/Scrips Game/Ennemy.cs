﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using MoreMountains.Feedbacks;

public class Ennemy : MonoBehaviour
{
    public float speedRun = 1f;
    public float durationJump = 0.5f;
    public float maxDistanceToBall = 1f;
    public float minDistanceToBall = 0f;
    private float Zoffest = 0f;

    public bool lookAtPlayer = true;
    public bool movementAB = false;
    private bool isJumping = false;

    [ShowIf("movementAB")]
    public Transform pointB;
    private Vector3 pointBOriginalPosition;
    [ShowIf("movementAB")]
    public float duration = 1f;
    private Tween movement;

    [ShowIf("movementAB")]
    public bool moveLeft = false;

    public Transform hand;

    public GameObject ballonMain;
    private bool catchPlayer = false;
    private Animator animator;
    private SphereCollider sphereCollider;

    private Quaternion rotationLooking;
    private Quaternion rotationJumping;
    private float lerpRotation = 0f;
    public Transform jumpDirection;
    public MMFeedbacks emoticoneLoseFeedback;
    public MMFeedbacks emoticoneWinFeedback;



    private void Awake() {
        animator = GetComponent<Animator>();
        sphereCollider = GetComponent<SphereCollider>();
        pointBOriginalPosition = pointB.transform.position;
        pointB.transform.parent = null;

        if(movementAB)
            animator.SetBool("Movement", true);

    }

    private void Start() {
        // Move
        if(movementAB){
            movement = transform.DOMove(pointBOriginalPosition, duration).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear).OnStepComplete(() =>{
                moveLeft = !moveLeft;
                animator.SetBool("Togle Step", moveLeft);
            });
        }
    }


    public void CatchPlayer(){
        if(GameControlleur.Instance.playerMovement.isSuperJump == false && catchPlayer == false && GameControlleur.Instance.gamefinish == false){
            animator.SetTrigger("Catch");
            emoticoneWinFeedback.PlayFeedbacks();
            GameControlleur.Instance.playerMovement.GetInEnnemyHand(hand);
            GameControlleur.Instance.LoseGame();
            movement.Kill();
            
            catchPlayer = true;
            ballonMain.SetActive(true);
            emoticoneLoseFeedback.StopFeedbacks();

            foreach (Transform particleSystem in emoticoneLoseFeedback.transform)
            {
                if(particleSystem.GetComponent<ParticleSystem>()){
                    particleSystem.GetComponent<ParticleSystem>().Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
                }
            }
        }
    }

    private void CalcultateOffset(){
        Transform playerTransform = GameControlleur.Instance.playerMovement.transform;

        //Offset Calculation
        Vector3 pointA = transform.position - (Vector3.forward * sphereCollider.radius * transform.lossyScale.x);
        float distanceToBall = Vector3.Distance(pointA, playerTransform.position);

        float ratioDistance = Mathf.InverseLerp(minDistanceToBall, maxDistanceToBall, distanceToBall);
        Zoffest = Mathf.Lerp(minDistanceToBall, maxDistanceToBall, ratioDistance);

        Vector3 lookatPointJumping = playerTransform.position.SetY(transform.position.y).SetZ(playerTransform.position.z + Zoffest) - transform.position;
        rotationJumping = Quaternion.LookRotation(lookatPointJumping);
        jumpDirection.rotation = rotationJumping;

    }

    private void Update() {
        Transform playerTransform = GameControlleur.Instance.playerMovement.transform;

        // Look Player
        if(!isJumping){
            Vector3 lookatPointLooking = playerTransform.position.SetY(transform.position.y) - transform.position;
            rotationLooking = Quaternion.LookRotation(lookatPointLooking);
        }

        if(lookAtPlayer){
            transform.rotation = rotationLooking;
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(other.tag.Equals("Player")){
            // Ne fais que des prise en avant
            if(transform.position.z > other.transform.position.z){
                DashTheBall(other);
            }
        }
    }

    public Collider detectionPlayer;
    private void DashTheBall(Collider ball){
        lookAtPlayer = false;
        isJumping = true;
        CalcultateOffset();
        movement.Kill();
        animator.SetBool("Run", true);
        Vector3 jumpPosition = jumpDirection.forward * (sphereCollider.radius + 3f);
        transform.DOMove(jumpPosition, durationJump).SetEase(Ease.InSine).SetRelative(true).OnUpdate(() =>{
            lerpRotation += (Time.deltaTime / durationJump) * 5;
            transform.rotation = Quaternion.Lerp(rotationLooking, rotationJumping, lerpRotation);
            if(transform.position.z < ball.transform.position.z){
                emoticoneLoseFeedback.PlayFeedbacks();
            }
        }).OnComplete(() =>{
            detectionPlayer.enabled = false;
            
        });
    }

    private void OnTriggerExit(Collider other) {
        if(other.tag.Equals("Player")){
            //Start();
            //animator.SetBool("Run", false);
        }
    }

    private void OnTriggerStay(Collider other) {
        // Course le joueurs
        if(other.tag.Equals("Player")){
            // For Run behaviour

            // Vector3 direction = (transform.position - other.transform.position.SetY(transform.position.y)).normalized;
            // transform.Translate(Vector3.forward * speedRun);
        }
    }

}
