﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public enum finishMode{
    Perfect,
    Normal,
    Fail,
}
public class FinishCube : MonoBehaviour
{
    public finishMode finishModeCube;
    public Color hightlightColor;
    [ReadOnly] public Color originalColor;
    private MeshRenderer meshRenderer;
    public float speedLerp = 1f;
    private float lerpValue = 0f;

    private void Awake() {
        meshRenderer = GetComponent<MeshRenderer>();
        originalColor = meshRenderer.material.GetColor("_Color");
    }

    private void Update() {
        lerpValue -= Time.deltaTime * speedLerp;
        lerpValue = Mathf.Clamp(lerpValue, 0f, 1f);
        
        meshRenderer.material.SetColor("_Color", Color.Lerp(originalColor, hightlightColor, lerpValue));
    }

    public void ChangeColor(){
        lerpValue += (Time.deltaTime * speedLerp) * 2f;
    }
}
