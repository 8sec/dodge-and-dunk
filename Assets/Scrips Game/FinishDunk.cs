﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FinishDunk : MonoBehaviour
{
    public GameObject arrow;
    [SerializeField] private DOTweenAnimation dOTweenAnimationArrow;
    private bool touch = false;
    public float maxfinishDuration = 5f;
    private float cooldownFinishDuration = 0f;

    private void Awake() {
        gameObject.SetActive(false);
    }

    public void Start() {
        dOTweenAnimationArrow.DOPlay();
        GameControlleur.Instance.finishCameraZoomIN_Feedback.PlayFeedbacks();
    }

    private void Update() {
        cooldownFinishDuration += Time.deltaTime;

        GameControlleur.Instance.playerMovement.canMove = false;
        RaycastHit raycastHit;
        touch = Physics.Raycast(arrow.transform.position, -arrow.transform.up, out raycastHit, 1f);

        if(touch){
            raycastHit.transform.GetComponent<FinishCube>().ChangeColor();
        }

        if(Input.GetMouseButtonDown(0) || cooldownFinishDuration >= maxfinishDuration){
            dOTweenAnimationArrow.DOPause();
            DebugPlus.DrawRay(arrow.transform.position, -arrow.transform.up).Duration(2f).Color(Color.red);
            Debug.Log("&&& touch: " + touch);

            if(touch){
                finishMode finish = raycastHit.transform.GetComponent<FinishCube>().finishModeCube;
                Debug.Log("&&& finish: mode = " + finish.ToString());
                GameControlleur.Instance.playerMovement.FinishDunk(finish);
            }
            gameObject.SetActive(false);
        }
    }


}
