﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using MoreMountains.Feedbacks;

public class Platforme : MonoBehaviour
{
    private bool destroyPlatformOnJump = false;

    [Title("Settings")]
    public bool isTrampoline = false;
    public bool isFinish = false;

    [ReadOnly] public bool isDestroy = false;
    public LayerMask layerMaskPlatforme;
    public List<PlayerMovement> runners;
    public float radiusSphereCastDetectionRunner;
    public LayerMask layerRunners;
    [Tooltip("Only for finish platforme")] public int multiplicateurValue = 0;

    [Title("Feedback Reference")]
    public MMFeedbacks jumpFeedback;
    public MMFeedbacks jumpFeedback_destructivePlatform;
    public MMFeedbacks jumpFeedback_PerfectSpot;

    public MMFeedbacks superJumpFeedback;



    public void InitPlatform(){
        CheckIfPlatformIsTrampoline();
        destroyPlatformOnJump = GameControlleur.Instance.destructivePlatform;
    }

    public void JumpOnPlatform(){
        if(destroyPlatformOnJump){
            if(jumpFeedback_destructivePlatform)
                jumpFeedback_destructivePlatform.PlayFeedbacks();
            isDestroy = true;
        }else{
            if(jumpFeedback)
                jumpFeedback.PlayFeedbacks();
        }
    }

    public void JumpOnPerfectSpot(){
        if(jumpFeedback_PerfectSpot){
            jumpFeedback_PerfectSpot.PlayFeedbacks();
        }
    }

    public void LandWithSuperJump(){
        if(superJumpFeedback)
            superJumpFeedback.PlayFeedbacks();
    }


    private void CheckIfRunnerIsOnPlatform(){
        runners.Clear();

        RaycastHit[] raycastHit;
        
        //SphereCast
        raycastHit = Physics.SphereCastAll(transform.position , radiusSphereCastDetectionRunner * transform.localScale.x ,Vector3.up, 2f * transform.localScale.y, layerRunners);
        if(raycastHit.Length >= 0){
            for (int i = 0; i < raycastHit.Length; i++)
            {
                runners.Add(raycastHit[i].transform.GetComponent<PlayerMovement>());
            }
        }

    }


    private void CheckIfPlatformIsTrampoline(){
        RaycastHit[] raycastHit;
        raycastHit = Physics.RaycastAll(transform.position, Vector3.up, 10f, layerMaskPlatforme);

        for (int i = 0; i < raycastHit.Length; i++)
        {
            if(raycastHit[i].transform.tag.Equals("Trampoline")){
                isTrampoline = true;
                return;
            }
        }
        // Si aucun trampoline n'a été trouver, alors il n'y en a pas
        isTrampoline = false;
    }

}
