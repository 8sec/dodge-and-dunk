﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using TMPro;
using MoreMountains.Feedbacks;
using MoreMountains.NiceVibrations;

public class PlayerMovement : MonoBehaviour
{
    [Title("Limits")]
    [SerializeField] private float limitX; 

    [Title("Settings Jump", "Change normal jump behaviour")]
    [PropertyTooltip("Move *value* unit per seconds")] public float speedForward = 1f;
    private float originalSpeedForwad;
    [PropertyTooltip("Speed of jump movement")] public float durationMovement = 1f;
    private float originalDurationMovement;
    [PropertyTooltip("Speed of jump movement")] public float jumpHeight = 3f;
    private float originaljumpHeight;

    [Title("Settings Super Jump", "Change super jump behaviour")]
    [PropertyTooltip("Move *value* unit per seconds")] public float superSpeedForward = 1f;
    [PropertyTooltip("Speed of jump movement")] public float superDurationMovement = 1f;
    [PropertyTooltip("Speed of jump movement")] public float superJumpHeight = 3f;

    [Title("Settings Final Jump", "Change jump for dunk behaviour")]
    [PropertyTooltip("Move *value* unit per seconds")] public float finalSpeedForward = 1f;
    [PropertyTooltip("Speed of jump movement")] public float finalDurationMovement = 1f;
    [PropertyTooltip("Speed of jump movement")] public float finalJumpHeight = 3f;

    [Title("Settings Giant PowerUp")]
    public float durationGiantPowerUp = 3f;
    public float giantSpeedForward = 1f;
    private float tmpCooldownGiantPowerUp;
    public float giantScale = 2f;
    private Vector3 originalScale;
    public float giantDurationMovement = 1f;
    public float giantJumpheight = 3f;


    [Space]
    [Title("Settings Movement Player")]
    public float speedMovementPlayer = 1f;
    public float speedMovementInput = 1f;
    public float radiusSphereCastDetectionPlatform = 1f;

    [Space]
    [Title("Settings Rotation Player")]
    public float speedRotationInput = 1f;
    public float speedRotationPlayer = 1f;
    public float maxAngle = 30f;
    public float minAngle = -30f;
    public Transform perssonageParent;

    [Space]
    [Title("Fire Mode")]
    public int numberOfChainPerfectSpotToActivateFireMode = 5;
    public int numberOfPlatformToJump = 5;
    public float durationMovementDuringFireMode = 5f;
    public float jumpHeightOutFireMode = 5f;
    public float durationMovementOutFireMode = 5f;

    [SerializeField] [ReadOnly] private int tmp_platformJumpOnFireMode = 0;
    [SerializeField] [ReadOnly] private int numberOfChainPerfectSpot = 0;

    
    [Space]
    [Title("Other Settings", "You should't change that value")]
    public string pseudo;
    public float durationFallInWater = 1f;
    public float forceProjectionWhenKilled = 20f;
    private float originalDurationFallInWater;
    public LayerMask layerPlatform;


    [Title("Control value", "Inspecting game")]
    [SerializeField] [ReadOnly] public bool canMove = false;
    [SerializeField] [ReadOnly] public bool killed = false;
    [SerializeField] [ReadOnly] public bool isSuperJump = false;
    [SerializeField] [ReadOnly] public bool isGiant = false;
    [SerializeField] [ReadOnly] public bool isOnFireMode = false;
    [SerializeField] [ReadOnly] public bool isOnFinisher = false;


    [ReadOnly] public bool isPlayer = true;

    [ReadOnly] public int positionInTheRace = 0;


    [Title("Reference")]
    public List<Platforme> allPlatform;

    public Transform targetPosition;
    public TouchControlleur touchControlleur;
    public TextMeshPro racePositionText;
    public Transform pogoStick;
    public ParticleSystem rescueFX;
    public MMFeedbacks superJumpFeedback;
    public MMFeedbacks fallFeedback;
    public MMFeedbacks jumpPerfectSpotFeedback;
    public MMFeedbacks InFireModeFeedback;
    public MMFeedbacks OutFireModeFeedback;
    public TextMeshProUGUI playerPositionGamePlayUI;
    public RagdollControlleur ragdollControlleur;
    public SpriteRenderer circleFireMode;

    [Title("Reference BasketBall")]
    public Transform cameraParentFinish;
    public Transform followZParent;
    public FinishDunk finishDunkHUD;
    public BasketHoop basketHoop;
    public Transform basketHoopPerfectPos;
    public Transform basketHoopNormalPos;

    public FollowPlayer followPlayerCamera;
    public MMFeedbacks jumpFeedback;
    public MMFeedbacks impactFeedback;




    private Sequence jumpSequence; 
    private Tween jump;
    private Tween fall;
    private float startY;
    private Tween tweenRotationY;



    public virtual void Awake() {
        if(targetPosition)
            targetPosition.parent = null;
        
        startY = transform.position.y;
        isSuperJump = false;

        originalSpeedForwad = speedForward;
        originaljumpHeight = jumpHeight;
        originalDurationMovement = durationMovement;
        originalScale = transform.lossyScale;
        originalDurationFallInWater = durationFallInWater;


        if(transform.tag.Equals("Player")){
            isPlayer = true;

            ragdollControlleur = GetComponentInChildren<RagdollControlleur>();
            if(ragdollControlleur)
                ragdollControlleur.ToogleRagdool(false);
        }else{
            isPlayer = false;
        }
        
    }


    public virtual void InitPlatformDetection() {
        Platforme[] allPlatformArray = GameControlleur.Instance.GetAllPlatform();
        foreach (var platform in allPlatformArray)
        {
            allPlatform.Add(platform);
        }
    }

    public virtual void Start() {
        InitPlatformDetection();
        UpdateSignFireMode();
    }

    public virtual void StartMoving()
    {
        canMove = true;
        MoveAndJump();
        tweenRotationY = pogoStick.DOLocalRotate(new Vector3(360f, 0f, 0f), 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Incremental).SetRelative(true);
    }

    void MoveAndJump(){
        if(canMove){

            jumpSequence = DOTween.Sequence();
            
            //Jump part
            jumpSequence.Append(transform.DOMoveZ(speedForward / 2, durationMovement / 2).SetRelative(true).SetEase(Ease.Linear)
            .OnStart(() =>{
                jump = pogoStick.DOMoveY(jumpHeight, durationMovement / 2).SetRelative(true).SetEase(Ease.OutQuad);
            })
            .OnComplete(() =>{
                jumpFeedback.PlayFeedbacks();
                jump.Kill(true);
            }));

            //Fall part
            jumpSequence.Append(transform.DOMoveZ(speedForward / 2, durationMovement / 2).SetRelative(true).SetEase(Ease.Linear)
            .OnStart(() =>{
                fall = pogoStick.DOMoveY(startY, durationMovement / 2).SetEase(Ease.InQuad);
            })
            .OnComplete(() =>{
                impactFeedback.PlayFeedbacks();
                fall.Kill(true);
                CheckIfGronded();
                MoveAndJump();
            }));
            
        }

    }

    public GameObject fakeshadow;
    public void GetInEnnemyHand(Transform hand){
        jumpSequence.Kill();
        tweenRotationY.Kill();
        fakeshadow.SetActive(false);
        pogoStick.rotation = Quaternion.identity;
        transform.DOMove(hand.position, 0.05f);
        targetPosition.DOMove(hand.position, 0.05f).OnComplete(() => {
            pogoStick.gameObject.SetActive(false);
        });
    }

    public float forceLancerFail = 10f;
    public Transform perfectDunkPos;
    public void FinishDunk(finishMode _finishMode){
        jumpSequence.Kill();
        cameraLookAtBasket = false;
        canMove = false;

        followPlayerCamera.enabled = true;
        followPlayerCamera.transform.parent = null;
        followPlayerCamera.target = pogoStick;
        followPlayerCamera.offset = followPlayerCamera.offset.SetX(pogoStick.position.x);
        followPlayerCamera.LookAtTarget = true;
                
        switch(_finishMode){
            case finishMode.Normal:
                VibrationManager.VibrateMedium();
                pogoStick.DOJump(basketHoopPerfectPos.transform.position, 2f, 1, 1.8f).SetEase(Ease.OutQuad).OnComplete(() => {
                basketHoop.normalDunk.PlayFeedbacks();
                pogoStick.DOKill();
                pogoStick.DOMoveY(0f, 0.6f).SetEase(Ease.Linear).OnComplete(() =>{ 
                    GameControlleur.Instance.WinGame(); 
                    GameControlleur.Instance.finishCameraZoomOUT_Feedback.PlayFeedbacks();
                    pogoStick.DOKill();
                    });
                // DebugPlus.LogOnScreen("Normal Dunk").Duration(4f);
                });

            break;

            case finishMode.Perfect:
                VibrationManager.VibrateHeavy();
                basketHoop.vignetteFeedback.PlayFeedbacks();
                pogoStick.DOJump(perfectDunkPos.position, 9f, 1, 2f).OnComplete(() =>{
                basketHoop.perfectDunk.PlayFeedbacks();
                pogoStick.DOMoveY(0f, originalDurationMovement / 4).SetDelay(0.15f).SetEase(Ease.InQuad).OnComplete(() =>{ 
                    GameControlleur.Instance.WinGame();
                    GameControlleur.Instance.finishCameraZoomOUT_Feedback.PlayFeedbacks();
                    pogoStick.DOKill();
                    
                    });
                });
                    
                // DebugPlus.LogOnScreen("Super Dunk").Duration(4f);
            break;

            case finishMode.Fail:
                VibrationManager.VibrateLight();
                pogoStick.DOKill();
                Rigidbody rigidbodyBallon = pogoStick.GetComponent<Rigidbody>();
                rigidbodyBallon.isKinematic = false;
                rigidbodyBallon.useGravity = true;
                Vector3 direction = basketHoopPerfectPos.transform.position.SetY(0) - rigidbodyBallon.position;
                rigidbodyBallon.AddForce(direction * forceLancerFail);
                StartCoroutine(LoseInFailDunk(2f));
            
            
                /*
                pogoStick.DOJump(basketHoopPerfectPos.transform.position.SetY(0).SetX(Random.Range(-5f, 5f)), 3f, 1, 1.5f).OnComplete(() => basketHoop.failDunk.PlayFeedbacks()).Append(pogoStick.DOMoveY(startY, originalDurationMovement / 2).SetEase(Ease.InQuad)).OnComplete(() =>{
                    GameControlleur.Instance.WinGame();
                    pogoStick.DOKill();
                    followPlayerCamera.enabled = false;
                    //pogoStick.DOMoveY(0.5f, originalDurationMovement).SetRelative(true).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutQuad);
                    GameControlleur.Instance.finishCameraZoomOUT_Feedback.PlayFeedbacks();
                    });
                */
            break;
        }
        
    }

    public IEnumerator LoseInFailDunk(float waitDuration){
        yield return new WaitForSecondsRealtime(waitDuration);
        basketHoop.failDunk.PlayFeedbacks();
        GameControlleur.Instance.WinGame();
        followPlayerCamera.enabled = false;
        GameControlleur.Instance.finishCameraZoomOUT_Feedback.PlayFeedbacks();
    }

    public virtual void CheckIfGronded(){
        RaycastHit raycastHit;
        
        //SphereCast
        // DebugPlus.DrawSphere(transform.position.SetY(1), radiusSphereCastDetectionPlatform).Duration(100f).Color(Color.red);
        bool hitPlatform = Physics.SphereCast(transform.position.SetY(startY + 2),radiusSphereCastDetectionPlatform ,Vector3.down, out raycastHit, 3, layerPlatform);

        bool justLandWithSuperJump = false;
        if(hitPlatform){
            
            if(isSuperJump){
                NormalJump();
            }

            if(raycastHit.transform.tag.Equals("Finish")){
                FinalJump();
            }
            

            if(raycastHit.transform.tag.Equals("Trampoline")){
                if(transform.tag.Equals("Player")){
                    if(superJumpFeedback)
                        superJumpFeedback.PlayFeedbacks();
                }
                raycastHit.transform.GetComponent<DOTweenAnimation>().DOPlay();
                raycastHit.transform.GetComponent<MMFeedbacks>().PlayFeedbacks();

                SuperJump();
            }

        }
        
    }

    public virtual void DetectNextPlatform(){

        List<Platforme> availablePlatforme = new List<Platforme>();

        // Ne récupere que les plaformes qui sont sur la ligne suivante
        for (int i = 0; i < allPlatform.Count; i++)
        {
            bool platformIsOnTheNextRow = (Mathf.Round(allPlatform[i].transform.position.z) == Mathf.Round(transform.position.z + speedForward));
            if(platformIsOnTheNextRow && allPlatform[i].isDestroy == false){
                availablePlatforme.Add(allPlatform[i]);
            }    
        }
        
        // Choisis la platforme la plus proche de l'IA
        Platforme nextPlatform = GetCloserPlatforme(availablePlatforme);

        // Si la platforme est un trampoline, on evite de le prendre
        if(nextPlatform.isTrampoline){
            availablePlatforme.Remove(nextPlatform);
            nextPlatform = GetCloserPlatforme(availablePlatforme);
        }

        MovePlayerToNextPlatform(nextPlatform.transform);   
    }

    public void MovePlayerToNextPlatform(Transform platform){
        if(canMove && isOnFireMode){
            transform.DOMoveX(platform.position.x, durationMovement);
            targetPosition.DOMoveX(platform.position.x, durationMovement);
        }
    }

    private void UpdateSignFireMode(){
        if(circleFireMode){
            float remapPlatformJump = 0;
            if(isOnFireMode){
                // Let value to 0 = full filling
            }else{
                remapPlatformJump = MathFunction.Remap((float) numberOfChainPerfectSpot, 0f, numberOfChainPerfectSpotToActivateFireMode, 360f, 0f);
            }
            circleFireMode.material.DOFloat(remapPlatformJump, "_Arc1", durationMovementDuringFireMode);
        }
    }

    public void ToogleFireMode(bool state){
        if(state == true){
            isOnFireMode = true;
            numberOfChainPerfectSpot = 0;
            tmp_platformJumpOnFireMode = 0;
            durationMovement = durationMovementDuringFireMode;
            InFireModeFeedback.PlayFeedbacks();
            GameControlleur.Instance.ShowVignetteFireMode(true);
        }

        if(state == false){
            isOnFireMode = false;
            tmp_platformJumpOnFireMode = 0;
            jumpHeight = jumpHeightOutFireMode;
            durationMovement = durationMovementOutFireMode;
            OutFireModeFeedback.PlayFeedbacks();
            UpdateSignFireMode();
            GameControlleur.Instance.ShowVignetteFireMode(false);
        }
    }

    public void EatGiantPowerUp(){
        transform.DOScale(giantScale, 1f);
        tmpCooldownGiantPowerUp = 0f;
        durationFallInWater = durationFallInWater / 2f;
        GiantJump();
    }

    public void EndGiantPowerUp(){
        transform.DOScale(originalScale, 1f);
        durationFallInWater = originalDurationFallInWater;
        isGiant = false;
    }

    private void GiantJump(){
        isGiant = true;
        isSuperJump = true;
        speedForward = giantSpeedForward;
        jumpHeight = giantJumpheight;
        durationMovement = giantDurationMovement;
    }

    private void CooldownGiant(){
        tmpCooldownGiantPowerUp += Time.deltaTime;

        if(tmpCooldownGiantPowerUp > durationGiantPowerUp){
            EndGiantPowerUp();
        }
    }

    private void SuperJump(){
        isSuperJump = true;
        speedForward = superSpeedForward;
        jumpHeight = superJumpHeight;
        durationMovement = superDurationMovement;
        if(isPlayer)
            MMVibrationManager.ContinuousHaptic(0.25f, 0f, superDurationMovement, HapticTypes.None, this, this);
    }

    private void FinalJump(){
        isSuperJump = true;
        speedForward = finalSpeedForward;
        jumpHeight = finalJumpHeight;
        durationMovement = finalDurationMovement;
        cameraLookAtBasket = true;
        finishDunkHUD.gameObject.SetActive(true); // Activate HUD
        
        tweenRotationY.Kill();
        pogoStick.DORotate(Vector3.zero, 0.1f);

        followPlayerCamera.transform.parent = cameraParentFinish;
        followPlayerCamera.enabled = false;
        followPlayerCamera.transform.DOLocalMove(Vector3.zero, 0.5f);
    }

    private float lerpCameraLookAt = 0f;
    private bool cameraLookAtBasket = false;
    private void CameraLookAtBasket(){
        Vector3 relativePos = basketHoop.transform.position.SetY(followZParent.position.y) - followZParent.transform.position;
        Quaternion lookBasket = Quaternion.LookRotation(relativePos);
        followZParent.rotation = Quaternion.Lerp(Quaternion.identity, lookBasket, lerpCameraLookAt);
        //cameraParentFinish.rotation = Quaternion.Lerp(Quaternion.identity, lookBasket, lerpCameraLookAt);
        
        lerpCameraLookAt += Time.deltaTime;
    }
    
    private void NormalJump(){
        isSuperJump = false;
        speedForward = originalSpeedForwad;
        jumpHeight = originaljumpHeight;
        durationMovement = originalDurationMovement;

        if(isOnFireMode){
            durationMovement = durationMovementDuringFireMode;
        }
        
        
    }

    public virtual void Win(){
        canMove = false;
        jumpSequence.Kill();
        ResetRotation();
        NormalJump();
    }

    public void ResetRotation(){
        perssonageParent.DORotate(Vector3.zero, 1f); // Reset Rotation
    }


    public Platforme GetCloserPlatforme(List<Platforme> listPlatforme){

        float shorterDistance = float.MaxValue;
        Platforme nextPlatform = null;

        for (int i = 0; i < listPlatforme.Count; i++)
        {
            float distance = Vector3.Distance(transform.position, listPlatforme[i].transform.position);
            if(distance < shorterDistance){
                shorterDistance = distance;
                nextPlatform = listPlatforme[i];
            }
        }
        return nextPlatform;
    }

    public virtual void Lose(){
        jumpSequence.Kill();
        pogoStick.DOMoveY(startY, durationMovement / 2).SetEase(Ease.InQuad);
        canMove = false;        
    }

    public virtual void Update() {
        Vector2 directionInputPlayer = touchControlleur.GetInputFromLeanTouch();


        if(canMove){
            if(isOnFireMode == false){
                MovementOnXAxisPlayer(directionInputPlayer);
            }
        }

        if(cameraLookAtBasket){
            CameraLookAtBasket();
        }
    }

    void MovementOnXAxisPlayer(Vector2 direction){
        Vector3 directionInput;

        // Lean Touch Method
        directionInput = direction;


        
        // Move Target
        float stepInput = speedMovementInput * Time.deltaTime;
        Vector3 newInputPosition = Vector3.MoveTowards(Vector3.zero, directionInput, stepInput);
        targetPosition.Translate(newInputPosition);
        
        // Limits X
        float x = targetPosition.position.x;
        x = Mathf.Clamp(x, - limitX, limitX);
        targetPosition.position = targetPosition.position.SetX(x);

        // Move Player
        transform.position = Vector3.Lerp(transform.position, transform.position.SetX(targetPosition.position.x), Time.deltaTime * speedMovementPlayer);

        RotateOnXAxisPlayer(directionInput.x);
    }

    void RotateOnXAxisPlayer(float Xdirection){        
        float rotationT = 0.5f + (Xdirection * speedRotationInput);
        float newRotationZ = Mathf.Lerp(maxAngle, minAngle, rotationT);
        Vector3 rotationPlayer = new Vector3(0f, 0f, newRotationZ);

        perssonageParent.rotation = Quaternion.Lerp(perssonageParent.rotation, Quaternion.Euler(rotationPlayer), Time.deltaTime * speedRotationPlayer);
        
    }

    public float GetStartYPosition(){
        return startY;
    }

    public float GetZPositon(){
        return transform.position.z;
    }

}
