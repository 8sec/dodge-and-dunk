﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using MoreMountains.Feedbacks;

public class GameControlleur : MonoBehaviourSingleton<GameControlleur>
{
    public PlayerMovement playerMovement;
    private ArtificialIntelligence[] allIAs;
    public List<GameObject> runners;
    public Material perfectSpotMaterial;

    private bool gameStart = false;
    private bool runnerSetup = false;
    FinishLine finishLine;

    public static int currentGoldPlayer;

    [Title("Perfect Spot")]
    public float durationBeforeFadeOutPerfectSpot = 2f;
    private float tmp_durationFadeOut = 0;
    private bool aphaFadeOut = true;

    [Title("Reference")]
    public Text goldValueTitleScreenText;
    public Text goldValueWinScreenText;
    public Text goldToCollectText;

    public RoadMap roadMap;
    public Image sliderProgression;
    public Image fireModeVignette;
    private int goldToAdd = 0;

    [Title("Feedbacks")]
    public MMFeedbacks slowMotionFeedback;
    public MMFeedbacks finishCameraZoomIN_Feedback;
    public MMFeedbacks finishCameraZoomOUT_Feedback;


    [Title("Custom rules of the game")]
    public bool destructivePlatform = false;
    public bool rescuePlayerWhenTheyFall = true;


    public bool gamefinish = false;


    // Start is called before the first frame update

    private void Awake() {
        LevelManager.Instance.LoadLevel();

        finishLine = FindObjectOfType<FinishLine>();

        currentGoldPlayer = PlayerPrefs.GetInt("goldPlayer", 0);

        Time.timeScale = 1f;
        UIController.Instance.ShowPanelInstantly(UIPanelName.TITLE_SCREEN);
        
    }

    public void AddGold(int goldToAdd){
        currentGoldPlayer += goldToAdd;
        PlayerPrefs.SetInt("goldPlayer", currentGoldPlayer);
    }

    public FinishLine GetFinishLine(){
        return finishLine;
    }

    private void Start() {
        Application.targetFrameRate = 60;
        MonetizationManager.Instance.ShowInterstitial();
    }

    private void SetRaceProgression(){
        sliderProgression.fillAmount = Mathf.InverseLerp(playerMovement.GetStartYPosition(), finishLine.transform.position.z, playerMovement.GetZPositon());
    }

    private void SetupRunner(){
        runners.Clear();
        if(playerMovement){
            runners.Add(playerMovement.gameObject);
        }

        foreach (ArtificialIntelligence IA in allIAs)
        {
            runners.Add(IA.gameObject);
        }

        runnerSetup = true;
    }

    public void StartGame()
    {
        GameManager.Instance.StartSession();
        if(playerMovement.isActiveAndEnabled){
            playerMovement.StartMoving();
        }

        gameStart = true;
    }

    public Platforme[] GetAllPlatform(){
        return GameObject.FindObjectsOfType<Platforme>();
    }

    private void Update() {
       
       
    }

    public PlayerMovement GetPlayer(){
        return playerMovement;
    }

    public void CalculateRunnerPosition(){
        runners.Sort(delegate(GameObject a, GameObject b){
            float distance_a = a.transform.position.z;
            float distance_b = b.transform.position.z;
            if (distance_a==distance_b) return 0;
            if (distance_a >= distance_b)
                return -1;  else  return 1;
        });

        
        for (int i = 0; i < runners.Count; i++)
        {
            runners[i].GetComponent<PlayerMovement>().positionInTheRace = i + 1;
        }
    }


    public void WinGame(){
        gamefinish = true;
        GameManager.Instance.LevelSuccess();
        playerMovement.Win();
    }

    public void CollectGold(){
        int newGold = currentGoldPlayer;
        int tmpValueGoldToPrint = newGold - goldToAdd;
        
        DOTween.To(() => tmpValueGoldToPrint, x => tmpValueGoldToPrint = x, newGold, 1.5f).OnUpdate(() =>{
            if(goldValueTitleScreenText){
                goldValueWinScreenText.text = tmpValueGoldToPrint.ToString();
            }
        }).OnComplete(() =>{
            LevelManager.Instance.LoadNextLevel();
        });

    }

    public void LoseGame(){
        gamefinish = true;
        playerMovement.Lose();
        slowMotionFeedback.PlayFeedbacks();
    }

    public ArtificialIntelligence[] GetAllIAs(){
        return allIAs;
    }

    public void ShowVignetteFireMode(bool state){
        switch(state){
            case true:
                fireModeVignette.DOFade(0.5f, 0.5f);
            break;

            case false:
                fireModeVignette.DOFade(0f, 0.5f);
            break;
        }
    }

    public void FadeOutAlphaSpot(bool state){

        switch(state){
            case true:
                perfectSpotMaterial.DOFade(0.4f, 0.5f);
                aphaFadeOut = true;
            break;

            case false:
                perfectSpotMaterial.DOFade(0.8f, 0.1f);
                tmp_durationFadeOut = 0f;
                aphaFadeOut = false;
            break;
        }
    }
}
