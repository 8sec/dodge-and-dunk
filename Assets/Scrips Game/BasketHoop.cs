﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using MoreMountains.Feedbacks;

public class BasketHoop : MonoBehaviour
{
    [Title("Reference")]
    public MMFeedbacks failDunk;
    public MMFeedbacks normalDunk;
    public MMFeedbacks perfectDunk;
    public MMFeedbacks vignetteFeedback;


    private void Start() {
        // failDunk.Initialization();
        // normalDunk.Initialization();
        // perfectDunk.Initialization();
    }
}
