﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum GDPRConsentNeed
{
    UNKNOWN,
    NEEDED,
    NOT_NEEDED
}

public enum GDPRConsentUserResponse
{
    UNKNOWN,
    CONSENT_GIVEN,
    CONSENT_DENIED
}


public class GDPRManager : MonoBehaviour
{

    public delegate void ConsentStateReady(bool consentState);

    public static event ConsentStateReady OnConsentStateReady;

    public static GDPRManager Instance;
    public GameObject UI_Consent_GO;

    public bool GiveConsentIfLocaleUnknown = true;

    const string Key_ConsentUserResponse = "Key_ConsentUserResponse";




    public GDPRConsentNeed ConsentNeed = GDPRConsentNeed.UNKNOWN;
    public GDPRConsentUserResponse UserConsentResponse = GDPRConsentUserResponse.UNKNOWN;

  public bool HasConsent
    {
        get
        {
             UserConsentResponse = (GDPRConsentUserResponse)PlayerPrefs.GetInt(Key_ConsentUserResponse, (int)GDPRConsentUserResponse.UNKNOWN);
            switch (UserConsentResponse)
            {
                case GDPRConsentUserResponse.UNKNOWN:
                    return GiveConsentIfLocaleUnknown;
                case GDPRConsentUserResponse.CONSENT_GIVEN:
                    return true;
                case GDPRConsentUserResponse.CONSENT_DENIED:
                    return false;
                default:
                    return GiveConsentIfLocaleUnknown;

            }
        }
    }

    public bool ConsentAsked
    {
        get
        {
            UserConsentResponse = (GDPRConsentUserResponse)PlayerPrefs.GetInt(Key_ConsentUserResponse, (int)GDPRConsentUserResponse.UNKNOWN);
            switch (UserConsentResponse)
            {
                case GDPRConsentUserResponse.UNKNOWN:
                    return false;
                case GDPRConsentUserResponse.CONSENT_GIVEN:
                    return true;
                case GDPRConsentUserResponse.CONSENT_DENIED:
                    return true;
                default:
                    return false;

            }
        }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(this);
            DisplayUI(false);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public static List<string> EU_CountryCodes =new List<string> { "BE", "BG", "CZ", "DK", "DE", "EE", "IE", "EL", "ES", "FR", "HR", "IT", "CY",
    "LV", "LT", "LU", "HU", "MT", "NL", "AT", "PL", "PT", "RO", "SI", "SK", "FI",
    "SE", "UK"};

    public bool CountryNeedsGDPRConsent(string countryCode)
    {
        return EU_CountryCodes.Contains(countryCode);
    }
   
    // Start is called before the first frame update
    void Start()
    {
#if UNITY_EDITOR
        Debug.LogError("Force check consent in editor");
        CheckForGDPRConsentNeed();
#endif
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CheckForGDPRConsentNeed()
    {
        UserConsentResponse = (GDPRConsentUserResponse)PlayerPrefs.GetInt(Key_ConsentUserResponse, (int)GDPRConsentUserResponse.UNKNOWN);
        if(UserConsentResponse == GDPRConsentUserResponse.UNKNOWN)
        {
            StartCoroutine(GetLocale());
        }
        else
        {
            if (OnConsentStateReady != null)
                OnConsentStateReady(HasConsent);
        }

    }


    IEnumerator GetLocale()
    {
        UnityWebRequest www = UnityWebRequest.Get("http://ip-api.com/json");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);

            // Or retrieve results as binary data
            byte[] results = www.downloadHandler.data;
            UserConsentResponse = GDPRConsentUserResponse.UNKNOWN;
            ConsentNeed = GDPRConsentNeed.NOT_NEEDED;

            try
            {

                Debug.LogError("Received : " + www.downloadHandler.text);
               //;: var res = JsonUtility.FromJson<GeoLocaleData>("{\"city\":\"Paris\", \"country\":\"France\", \"countryCode\":\"FR\", \"status\":\"success\"}");
                var res = JsonUtility.FromJson<GeoLocaleData>(www.downloadHandler.text);
                Debug.LogError("CC = " + res.countryCode);

                if (CountryNeedsGDPRConsent(res.countryCode)) {
                    ConsentNeed = GDPRConsentNeed.NEEDED;

                    UserConsentResponse = GDPRConsentUserResponse.CONSENT_DENIED;
                    Debug.LogError("NEED CONSENT. SHOW WINDOW");
                    DisplayUI(true);
                }
                else
                {
                    ConsentNeed = GDPRConsentNeed.NOT_NEEDED;
                    UserConsentResponse = GDPRConsentUserResponse.CONSENT_GIVEN;

                    PlayerPrefs.SetInt(Key_ConsentUserResponse, (int)GDPRConsentUserResponse.CONSENT_GIVEN);
                }



            }
            catch (System.Exception ex)
            {

                // TODO: Hook into an auto retry case

                Debug.LogError("Could not get geo data: " + ex.ToString());
            }



          
            
        }
    }


    public void SetConsent(bool consentGiven)
    {
        DisplayUI(false);

        if (consentGiven)
        {
            UserConsentResponse = GDPRConsentUserResponse.CONSENT_GIVEN;
            PlayerPrefs.SetInt(Key_ConsentUserResponse, (int)GDPRConsentUserResponse.CONSENT_GIVEN);

        }
        else
        {
            UserConsentResponse = GDPRConsentUserResponse.CONSENT_DENIED;
            PlayerPrefs.SetInt(Key_ConsentUserResponse, (int)GDPRConsentUserResponse.CONSENT_DENIED);
        }

        if (OnConsentStateReady != null)
            OnConsentStateReady(HasConsent);

    }

    public void DisplayUI(bool state)
    {
        UI_Consent_GO.SetActive(state);
     

    }

    public void AskConsent()
    {
        DisplayUI(true);
    }

    public void LearnMore()
    {
        Application.OpenURL("http://8sec.games/privacy");
    }

    /// <summary>
    /// The Geo data for a user.
    /// 
    /// http://ip-api.com/docs/api:json
    /// 
    /// <code>
    /// {
    /// 	"status": "success",
    /// 	"country": "COUNTRY",
    /// 	"countryCode": "COUNTRY CODE",
    /// 	"region": "REGION CODE",
    /// 	"regionName": "REGION NAME",
    /// 	"city": "CITY",
    /// 	"zip": "ZIP CODE",
    /// 	"lat": LATITUDE,
    /// 	"lon": LONGITUDE,
    /// 	"timezone": "TIME ZONE",
    /// 	"isp": "ISP NAME",
    /// 	"org": "ORGANIZATION NAME",
    /// 	"as": "AS NUMBER / NAME",
    /// 	"query": "IP ADDRESS USED FOR QUERY"
    /// }
    /// </code>
    /// 
    /// </summary>
    /// 
    [System.Serializable]
    public class GeoLocaleData
    {
        public string status;

        public string country;

        public string city;

        public string countryCode;
        public string region;
        public string regionName;

        public string zip;
        public string lat;
        public string lon;
        public string timezone;
        public string isp;
        public string org;
        
        public string query;
    }
}
